#!/usr/bin/env node
'use strict';
var fs = require('fs');
var meow = require('meow');
var path = require('path');

// Taken from http://www.andornot.com/blog/post/Replace-MS-Word-special-characters-in-javascript-and-C.aspx
var replace = function(text) {
	var s = text;
	// smart single quotes and apostrophe
	s = s.replace(/[\u2018\u2019\u201A]/g, "\'");
	// smart double quotes
	s = s.replace(/[\u201C\u201D\u201E]/g, "\"");
	// ellipsis
	s = s.replace(/\u2026/g, "...");
	// dashes
	s = s.replace(/[\u2013\u2014]/g, "-");
	// circumflex
	s = s.replace(/\u02C6/g, "^");
	// open angle bracket
	s = s.replace(/\u2039/g, "<");
	// close angle bracket
	s = s.replace(/\u203A/g, ">");
	// spaces
	s = s.replace(/[\u02DC\u00A0]/g, " ");

	return s;
}

var cli = meow({
	help: ['replace_ms_chars folderpath']
});

var folder = cli.input[0];

if (!folder) {
	folder = process.cwd();
}

var files = fs.readdirSync(folder);

files.forEach(function(file) {
	var filePath = folder + "/" + file;
	if(!fs.lstatSync(filePath).isFile())
		return;

	var data = replace(fs.readFileSync(filePath, 'utf8'));
	fs.writeFile(filePath, data, 'utf8', function(err){
		if(err)
			console.error(err);
		console.log(file + " - Checked")
	});
});