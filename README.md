# replace-ms-chars

Replace MS Word's special characters like smart quotes and hyphens with its original character.

## Installation

Download node at [nodejs.org](http://nodejs.org) and install it, if you haven't already.

```sh
npm i -g replace_msword_sp_chars_cli
```

## Usage

```sh

# Replace all files in current folder
replace_ms_chars

# Replace all files in target path
replace_ms_chars path\to\folder

```